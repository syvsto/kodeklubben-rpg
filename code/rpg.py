# Rommene er definert som . hvis de ikke har utgang
# og n (nord), s (sør), o (øst), v (vest) hvis de har
# en utgang i den retningen
rom = [[".",  "s",  "."],
       ["so", "nv", "."],
       ["no", "vo", "v"]]

spiller_posisjon = [3,3]


# Kan bruke denne til å forklare at man kan kalle sine egne funksjoner
def flytt_til_rom():
    "Flytt spilleren til et nytt rom hvis det er mulig"
    print("Du står i et mørkt rom i et gammelt fengsel.")
    les_rom_utganger(spiller_posisjon)
    velg_utgang(spiller_posisjon)

# Kan inneholde finnes_utgangen() frem til vi forklarer hvorfor man trenger
# funksjoner (deling av den samme funksjonaliteten mellom flere steder i
# koden). Kan også forklare if statements/programflyt.
def les_rom_utganger(spiller_posisjon):
    "Finn utgangene fra et rom"
    rom_definisjon = finn_rom_ved_spiller()
    for r in rom_definisjon:
        if finnes_utgangen(r, "nord"):
            print("Det er en dør mot nord i rommet.")
        elif finnes_utgangen(r, "sør"):
            print("Det er en dør mot sør i rommet.")
        elif finnes_utgangen(r, "øst"):
            print("Det er en dør mot øst i rommet.")
        elif finnes_utgangen(r, "vest"):
            print("Det er en dør mot vest i rommet.")

# Kan snakke om brukerinput her.
def velg_utgang(spiller_posisjon):
    "Velg en utgang fra rommet og gå til neste rom"
    print("Velg hvilken retning du vil gå:")
    retning = input()

    rom_definisjon = finn_rom_ved_spiller()
    if finnes_utgangen(rom_definisjon, retning):
        if retning == "nord":
            print("Du gikk mot nord.")
            spiller_posisjon[0]-=1
        if retning == "sør":
            print("Du gikk mot sør.")
            spiller_posisjon[0]+=1
        if retning == "vest":
            print("Du gikk mot vest.")
            spiller_posisjon[1]-=1
        if retning == "øst":
            print("Du gikk mot øst.")
            spiller_posisjon[1]+=1
    else:
        print("Du kan ikke gå dit!")

# Kan brukes for å forklare løkker og viktigheten av funksjoner sammen
# med les_rom_utganger().
def finnes_utgangen(rom_definisjon, retning):
    for bokstav in rom_definisjon:
        if bokstav == 'n' and retning == "nord":
            return True
        elif bokstav == 's' and retning == "sør":
            return True
        elif bokstav == 'o' and retning == "øst":
            return True
        elif bokstav == 'v' and retning == "vest":
            return True

    return False

# Kan forklare hvordan man kan bruke veldig enkel matte for å gjøre div.
# programmeringsting, og til å snakke mer om datastrukturer og typer (string, tall, liste).
def finn_rom_ved_spiller():
    "Finn rommet som finnes ved spillerens plassering"
    x = spiller_posisjon[0] - 1
    y = spiller_posisjon[1] - 1
    return rom[x][y]


def start_spill():
    "Brukes som et inngangspunkt til spillet"
    # TODO(syver): Legg til mer tekst/hjelp til å starte
    print("Velkommen til kodeklubb-spillet")
    while True:
        flytt_til_rom()
